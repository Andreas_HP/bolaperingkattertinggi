/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.urutanbolaperingkatertinggi;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class urutanbolaperingkattertinggi {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int n;  
        Scanner sc=new Scanner(System.in);  
        System.out.print("Masukan jumlah Array peringkat elemen yg anda inginkan: ");  
        n=sc.nextInt();  
        int[] arrayleaderboard = new int[n];  
        System.out.println("Masukan jumlah bola utk peringkat tertinggi saat ini: ");  
        for(int i=0; i<n; i++)  
        {  
          arrayleaderboard[i]=sc.nextInt();  
        } 
        
        int nilaitmp;
        for(int ix=1; ix<arrayleaderboard.length;ix++)
        {
            for(int iz=0;iz<arrayleaderboard.length-ix;iz++)
            {
                if(arrayleaderboard[iz]< arrayleaderboard[iz+1])
                {
                   nilaitmp = arrayleaderboard[iz];
                   arrayleaderboard[iz] = arrayleaderboard[iz+1];
                   arrayleaderboard[iz+1] = nilaitmp;
                    
                }
            }
            
        }
        
        String leaderboards="[";
        for (int i=0; i<n; i++)   
        {  
          if(i==0)  
              leaderboards = leaderboards+arrayleaderboard[i];
          else 
          leaderboards = leaderboards +","+arrayleaderboard[i];
        }

        leaderboards = leaderboards +"]";

        System.out.println("Leaderboards ="+leaderboards);
        
        System.out.print("Masukan jumlah ARRAY bola elemen penantang yg anda inginkan: ");  
        n=sc.nextInt();  
        int[] arrayplayer = new int[n];  
        System.out.println("Masukan jumlah bola pemain penantang: ");  
        
        for(int i=0; i<n; i++)  
        {  
          arrayplayer[i]=sc.nextInt();  
        } 
        
        String playerball="[";
        for (int i=0; i<n; i++)   
        {  
          if(i==0)  
              playerball = playerball+arrayplayer[i];
          else 
          playerball = playerball +","+arrayplayer[i];
        }

        playerball = playerball +"]";
        
        System.out.println("Player ="+playerball);

        int totalarraygabungan = arrayleaderboard.length + arrayplayer.length;
        int[] arraygabungan = new int[totalarraygabungan];
        
        System.arraycopy(arrayleaderboard, 0, arraygabungan, 0, arrayleaderboard.length);
        System.arraycopy(arrayplayer, 0, arraygabungan, arrayleaderboard.length, arrayplayer.length);
     
        /*
        for(int ix=0; ix<arrayleaderboard.length; ix++)
        {
            arraygabungan[ix]=arrayleaderboard[ix];
        }

        for(int ix=arrayleaderboard.length; ix<arrayleaderboard.length+arrayplayer.length; ix++)
        {
            arraygabungan[ix]=arrayplayer[ix];
        } 
*/
            
        for(int ix=1; ix<totalarraygabungan;ix++)
        {
            for(int iz=0;iz<totalarraygabungan-ix;iz++)
            {
                if(arraygabungan[iz]< arraygabungan[iz+1])
                {
                   nilaitmp = arraygabungan[iz];
                   arraygabungan[iz] = arraygabungan[iz+1];
                   arraygabungan[iz+1] = nilaitmp;
                    
                }
            }
            
        }
        
        //Arrays.stream(arraygabungan).distinct().toArray();
        
        String arraygabungantext="[";
        for (int i=0; i<totalarraygabungan; i++)   
        {  
          if(i==0)  
              arraygabungantext = arraygabungantext+arraygabungan[i];
          else 
          arraygabungantext = arraygabungantext +","+arraygabungan[i];
        }

        arraygabungantext = arraygabungantext +"]";

        System.out.println("peringkat tertinggi terbaru ="+arraygabungantext);

        int[] arraybarugabunganx = new int[arraygabungan.length];
        
        System.arraycopy(arraygabungan, 0, arraybarugabunganx, 0, arraygabungan.length);
  
        int length = arraybarugabunganx.length;  
        length = remove_Duplicate_Elements(arraybarugabunganx, length); 

        /*
        String arraybarugabungantext="[";
        for (int i=0; i<length; i++)   
        {  
          if(i==0)  
              arraybarugabungantext = arraybarugabungantext+arraybarugabunganx[i];
          else 
          arraybarugabungantext = arraybarugabungantext +","+arraybarugabunganx[i];
        }

        arraybarugabungantext = arraybarugabungantext +"]";

        System.out.println("arraybarugabungantext ="+arraybarugabungantext);
        
       */
        int[] arrayoutput = new int[arrayplayer.length];
        
        for(int ix=0; ix<arrayplayer.length;ix++)
        {
          for(int iz=0; iz<length; iz++)
          {
            if(arrayplayer[ix] == arraybarugabunganx[iz])
            {
               arrayoutput[ix] = iz+1;
            }
          }
        }
                
        String arrayoutputtext="[";
        for (int i=0; i<arrayoutput.length; i++)   
        {  
          if(i==0)  
              arrayoutputtext = arrayoutputtext+arrayoutput[i];
          else 
          arrayoutputtext = arrayoutputtext +","+arrayoutput[i];
        }

        arrayoutputtext = arrayoutputtext +"]";

        System.out.println("Output = "+arrayoutputtext);
    }
    
    public static boolean check(int[] arr, int num)
    {
        boolean flag = false;
        for(int i=0;i<arr.length; i++)
        {
            if(arr[i]==num)
            {
                flag = true;
                break;
            }
        }
        return flag;
    }
    
    public static int remove_Duplicate_Elements(int arr[], int n){  
        if (n==0 || n==1){  
            return n;  
        }  
        int[] tempA = new int[n];  
        int j = 0;  
        for (int i=0; i<n-1; i++){  
            if (arr[i] != arr[i+1]){  
                tempA[j++] = arr[i];  
            }  
         }  
        tempA[j++] = arr[n-1];       
        for (int i=0; i<j; i++){  
            arr[i] = tempA[i];  
        }  
        return j;  
    }  
    
}
